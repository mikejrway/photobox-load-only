﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.QuestionTime.Com.Models
{
    public class Answer
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}