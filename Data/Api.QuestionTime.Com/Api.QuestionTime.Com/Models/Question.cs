﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.QuestionTime.Com.Models
{
    public class Question
    {
        public Question()
        {
            Answers = new List<Answer>();
        }

        public int Id { get; set; }
        public string QuestionText { get; set; }
        public string Category { get; set; }
        public virtual List<Answer> Answers { get; set; }
        public int CorrectIndex { get; set; }
    }
}