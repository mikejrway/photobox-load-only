﻿using Api.QuestionTime.Com.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Api.QuestionTime.Com.DataAccess
{
    public class QuestionsContextInitializer : DropCreateDatabaseAlways<QuestionsContext>
    {
        protected override void Seed(QuestionsContext context)
        {
            context.Questions.Add(new Question {
                QuestionText = "What is the new var?",
                Category = "Easy",
                Answers = new List<Answer> () {
                    new Answer { Text = "let" },
                    new Answer { Text = "int" },
                    new Answer { Text = "string" },
                    new Answer { Text = "double" } },
                CorrectIndex = 0 });
            context.SaveChanges();
        }
    }
}