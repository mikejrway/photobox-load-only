﻿using Api.QuestionTime.Com.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Api.QuestionTime.Com.DataAccess
{
    public class QuestionsContext : DbContext
    {
        public QuestionsContext()
        {
            Database.SetInitializer(new QuestionsContextInitializer());
        }
        public DbSet<Question> Questions { get; set; }
    }
}