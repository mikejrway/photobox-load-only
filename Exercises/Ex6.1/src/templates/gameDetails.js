import * as UI from './UI';

export default function gameDetails(playerName) {
    return `<h1>Welcome to Question Time ${playerName}</h1>

${UI.scoreUI()}

${UI.questionUI('What is the new var?', 'let', 'int', 'string', 'double')}
`;
}
