import AWS from "aws-sdk";
import fs from 'fs';



async function importData() {
    // An array of promises so we can wait for them all to finish
    let promises = [];

    console.log("Importing questions into DynamoDB. Please wait.");
    let docClient = new AWS.DynamoDB.DocumentClient();

    // Load all of the questions from the JSON file
    let allQuestions = JSON.parse(fs.readFileSync('data/questions.json', 'utf8'));
    // For each question, create a set of parameters to push to the data store
    allQuestions.forEach( (question) => {
        // Create params here

        // Create a new promise for each question
        promises.push( new Promise((resolve, reject) => {
            // Call docClient.put here

        }));
    });
    return Promise.all(promises);

}

export default importData;