import AWS from "aws-sdk";

async function createTable() {
    let p = new Promise((resolve, reject) => {
        
        let dynamodb = new AWS.DynamoDB();

        let questionParams = {
            TableName: "Questions",
            KeySchema: [
                { AttributeName: "category", KeyType: "HASH" },  //Partition key
                { AttributeName: "questionText", KeyType: "RANGE" }  //Sort key
            ],
            AttributeDefinitions: [
                { AttributeName: "category", AttributeType: "S" },
                { AttributeName: "questionText", AttributeType: "S" }
            ],
            ProvisionedThroughput: {
                ReadCapacityUnits: 10,
                WriteCapacityUnits: 10
            }
        };




    });

    return p;
}

export default createTable;