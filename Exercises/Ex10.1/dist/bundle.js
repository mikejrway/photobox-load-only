/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("aws-sdk");

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_aws_sdk__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_table__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dynamo_import__ = __webpack_require__(4);
let runImport = (() => {
    var _ref = _asyncToGenerator(function* () {
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */])();
        try {
            yield __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__create_table__["a" /* default */])();
        } catch (e) {
            console.log("Error creating tables. Ignoring and attempting import");
        }
    });

    return function runImport() {
        return _ref.apply(this, arguments);
    };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }






runImport().catch(() => {});

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_aws_sdk__);


function configureAWS() {
    //var creds = // Create credentials here

    let creds = new __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.Credentials('akid', 'secret', 'session');

    __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.config.update({
        region: "us-west-2",
        endpoint: "http://localhost:8000",
        credentials: creds
    });
}

/* harmony default export */ __webpack_exports__["a"] = (configureAWS);

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_aws_sdk__);
let createTable = (() => {
    var _ref = _asyncToGenerator(function* () {
        let p = new Promise(function (resolve, reject) {

            let dynamodb = new __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.DynamoDB();

            let questionParams = {
                TableName: "Questions",
                KeySchema: [{ AttributeName: "category", KeyType: "HASH" }, //Partition key
                { AttributeName: "questionText", KeyType: "RANGE" //Sort key
                }],
                AttributeDefinitions: [{ AttributeName: "category", AttributeType: "S" }, { AttributeName: "questionText", AttributeType: "S" }],
                ProvisionedThroughput: {
                    ReadCapacityUnits: 10,
                    WriteCapacityUnits: 10
                }
            };

            dynamodb.createTable(questionParams, function (err, data) {
                if (err) {
                    let message = "Unable to create table. Error JSON:" + JSON.stringify(err, null, 2);
                    console.error(message);
                    reject(message);
                } else {
                    let message = "Created table. Table description JSON:" + JSON.stringify(data, null, 2);
                    console.log(message);
                    resolve(message);
                }
            });
        });

        return p;
    });

    return function createTable() {
        return _ref.apply(this, arguments);
    };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }



/* harmony default export */ __webpack_exports__["a"] = (createTable);

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_aws_sdk__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_fs__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_fs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_fs__);
let importData = (() => {
    var _ref = _asyncToGenerator(function* () {

        let promises = [];

        console.log("Importing questions into DynamoDB. Please wait.");
        let docClient = new __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.DynamoDB.DocumentClient();

        var allQuestions = JSON.parse(__WEBPACK_IMPORTED_MODULE_1_fs___default.a.readFileSync('data/questions.json', 'utf8'));
        allQuestions.forEach(function (question) {
            var params = {
                TableName: "Questions",
                Item: {
                    "category": question.category,
                    "questionText": question.questionText,
                    "correctIndex": question.correctIndex,
                    "answers": question.answers
                }
            };

            promises.push(new Promise((resolve, reject) => {
                docClient.put(params, function (err, data) {
                    if (err) {
                        let message = "Unable to add question ${question.questionText}. Error JSON: ${JSON.stringify(err, null, 2)}";
                        console.error(message);
                        reject(message);
                    } else {
                        console.log("PutItem succeeded:", question.questionText);
                        resolve("PutItem succeeded: ${question.questionText}");
                    }
                });
            }));
        });
        return Promise.all(promises);
    });

    return function importData() {
        return _ref.apply(this, arguments);
    };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }




/* unused harmony default export */ var _unused_webpack_default_export = (importData);

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ })
/******/ ]);
//# sourceMappingURL=bundle.js.map