import Question from './Question';
import MultiAnswerQuestion from './MultiAnswerQuestion';

let _score = 0, _total = 0,
    _question =
        new MultiAnswerQuestion('Which ES feature(s) use(s) ...?', [0,2],
            'rest parameters', 'module', 'spread operator', 'arrow function');
const PLAYER_NAME = 'Your Name';

let game = {
    get score() {
        return _score;
    },
    get total() {
        return _total;
    },
    get playerName() {
        return PLAYER_NAME;
    },
    get question() {
        return _question;
    },
    checkAnswer (answer) {
        if (_question.checkAnswer(answer)) {
            _score++;
        }
        _total++;
    }
};

export default game;