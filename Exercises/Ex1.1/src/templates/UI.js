import answerDetails from './answerDetails';
import categoryDetails from './categoryDetails';
import enterUserNameDetails from './enterUserNameDetails';
import gameDetails from './gameDetails';
import questionDetails from './questionDetails';
import scoreDetails from './scoreDetails';

export {
        answerDetails as answerUI,
        categoryDetails as categoryUI,
        enterUserNameDetails as enterUserNameUI,
        gameDetails as gameUI,
        questionDetails as questionUI,
        scoreDetails as scoreUI
};