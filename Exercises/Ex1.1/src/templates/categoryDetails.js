function categoryDetails(categories, selectedCategory) {
    return `<h4>Select a Category: <br><select class="form-control" name='category'>
        ${categories.map(item => `<option value='${item}' ${item == selectedCategory ? 'selected' : ''}>${item}</option>`).join('')}
    </select></h4>`;
}

export default categoryDetails;