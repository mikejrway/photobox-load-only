function questionDetails(questionText, type, ...answers) {
    return `<h3>${questionText}</h3>

    <div class="form-group">
      ${answers.map((item, index) => `<div class='${type}'>
        <label>
            <input type='${type}' name='answer' value='${index}'>
            ${item}
        </label>
      </div>`).join('')}
    </div>
    `
}

export default questionDetails;