import * as UI from './UI';
import gameStates from '../models/gameStates';
import MultiAnswerQuestion from '../models/MultiAnswerQuestion';

function gameDetails(game) {
    let {playerName, score, total, question, categories, selectedCategory} = game;
    let {questionText, answers} = question;
    let type = question instanceof MultiAnswerQuestion ? 'checkbox' : 'radio';

    return `

<div class="container">
    <div class="row">
        <div class="col-xs-offset-4 col-xs-4">
            <h1>Welcome to Question Time ${playerName}</h1>
            <div class="well">

                ${childUI()}

            </div>
        </div>
    </div>
</div>

`;

    function childUI() {
        switch(game.state) {
            case gameStates.ENTER_PLAYER_NAME:
                    return `${UI.enterUserNameUI()}`;
            case gameStates.SELECT_CATEGORY:
                return `${UI.categoryUI(categories, selectedCategory)}`;
            case gameStates.PLAY_GAME:
                return `${game.answerResult ? UI.answerUI(game.answerResult) : ''}

                ${UI.scoreUI(score, total)}

                ${game.over ? '<h2>Game Over</h2>' : UI.questionUI(questionText, type, ...answers)}`;
        }
    }
}



export default gameDetails;