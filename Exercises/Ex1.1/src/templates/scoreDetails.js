export default function scoreDetails(score = 0, total = 0) {
    const percent = Math.round(score * 100 / total);
    return `<h2>You have scored ${score} out of ${total} for ${Number.isNaN(percent) ? 0 : percent}%</h2>`;
}

