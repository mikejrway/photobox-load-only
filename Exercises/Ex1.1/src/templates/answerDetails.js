export default function answerDetails(answerResult) {
    return `<div class="alert ${answerResult == 'Wrong' ? 'alert-danger' : 'alert-success'}">
        <span class="glyphicon ${answerResult == 'Wrong' ? 'glyphicon-remove' : 'glyphicon-ok'}"></span>
        ${answerResult}
    </div>`;
}

