import mongoose from 'mongoose';

let Schema = mongoose.Schema;

let QuestionSchema = new Schema({
    questionText: String,
    correctIndex: Number,
    answers: [String]
});

export default mongoose.model('Question', QuestionSchema);