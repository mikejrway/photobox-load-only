function getData(url) {
    let prom = new Promise((resolved, rejected) => {
        let req = new XMLHttpRequest();
        req.open('GET', url);
        req.addEventListener('load', () => {
            if (req.status == '200') {
                resolved(req.responseText);
            } else {
                rejected(req.responseText);
            }
        });
        req.send();
    });
    return prom;
}

export default getData;