import gameStates from './gameStates';
import questionBank from './questionBank';
import {notify} from '../utilities/nb';
import wait from '../utilities/wait';

let _score = 0, _total = 0, _over = false,
    _answerResult = '', _state = gameStates.ENTER_PLAYER_NAME,
    _categories = [...questionBank.keys()],
    _selectedCategory = _categories[0],
    _questions = questionBank.get(_selectedCategory).values(),
    _question = _questions.next().value;
let _playerName = '';

let game = {
    get score() { return _score; },
    get state() { return _state; },
    get total() { return _total; },
    get answerResult() { return _answerResult; },
    get playerName() { return _playerName; },
    get question() { return _question; },
    get over() { return _over; },
    get categories() { return _categories; },
    get selectedCategory() { return _selectedCategory; },
    set category(category) {
        _selectedCategory = category;
        _questions = questionBank.get(_selectedCategory).values();
        _question = _questions.next().value;
        _state = gameStates.PLAY_GAME;
        notify(game);
    },
    set playerName(name) {
        _playerName = name;
        _state = gameStates.SELECT_CATEGORY;
        notify(game);
    },
    checkAnswer(answer) {
        let correct = _question.checkAnswer(answer);
        if (correct) {
            _score++;
        }
        _total++;
        wait(100)
            .then(() => {
                _answerResult = correct ? 'Correct!' : 'Wrong';
            })
            .then(() => {notify(game);})
            .then(wait)
            .then(() => {
                _answerResult = '';
            })
            .then(() => {notify(game);})
            .then(this.nextQuestion);
    },
    nextQuestion() {
        const item = _questions.next();
        if (item.done) {
            _over = true;
            notify(game);
            return;
        }
        _question = item.value;
        notify(game);
    }
};

//wait(2000)
//    .then(game.nextQuestion)
//    .then(wait)
//    .then(game.nextQuestion)
//    .then(wait)
//    .then(game.nextQuestion)
//    .then(wait);
export default game;