import render from './render';
import game from '../models/game';
import MultiAnswerQuestion from '../models/MultiAnswerQuestion';
import {addListener} from '../utilities/nb';
import {APP_TARGET, CHECK_ANSWER_BUTTON, CHANGE_CATEGORY_BUTTON, SET_PLAYER_BUTTON} from './domElements';

// listens for notify from game
addListener(game, () => {
    render();
});

// click events on buttons
CHECK_ANSWER_BUTTON.addEventListener('click', (e) => {
    e.preventDefault();
    if (game.question instanceof MultiAnswerQuestion) {
        const selectedCheckboxes = document.querySelectorAll('input[name=answer]:checked');
        const answer = [...selectedCheckboxes].map((item) => Number.parseInt(item.value));
        game.checkAnswer(answer);
    } else {
        const selectedRadio = document.querySelector('input[name=answer]:checked');
        const answer = Number.parseInt(selectedRadio.value);
        game.checkAnswer(answer);
    }
});

CHANGE_CATEGORY_BUTTON.addEventListener('click', (e) => {
    e.preventDefault();
    const selectedCategoryValue = document.querySelector('select[name=category] option:checked').value;
    game.category = selectedCategoryValue;
});

SET_PLAYER_BUTTON.addEventListener('click', (e) => {
    e.preventDefault();
    const name = document.querySelector('#player-name').value;
    game.playerName = name;
});