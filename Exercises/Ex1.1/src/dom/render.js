import game from '../models/game';
import gameStates from '../models/gameStates';
import {gameUI} from '../templates/UI';
import {APP_TARGET, CHECK_ANSWER_BUTTON, CHANGE_CATEGORY_BUTTON, SET_PLAYER_BUTTON} from './domElements';

function render() {
    APP_TARGET.innerHTML = gameUI(game);
    // these are necessary because they aren't in the rendered UI
    CHECK_ANSWER_BUTTON.disabled = game.answerResult || game.over;
    CHECK_ANSWER_BUTTON.style.display = game.state == gameStates.PLAY_GAME ? 'block' : 'none';
    CHANGE_CATEGORY_BUTTON.style.display = game.state == gameStates.SELECT_CATEGORY ? 'block' : 'none';
    SET_PLAYER_BUTTON.style.display = game.state == gameStates.ENTER_PLAYER_NAME ? 'block' : 'none';
}
// this does an initial render, after this, notify takes over in the game
render();
export default render;