const APP_TARGET = document.getElementById('es-app');
const CHECK_ANSWER_BUTTON = document.getElementById('check-answer-button');
const CHANGE_CATEGORY_BUTTON = document.getElementById('change-category-button');
const SET_PLAYER_BUTTON = document.getElementById('set-player-button');

export {APP_TARGET, CHECK_ANSWER_BUTTON, CHANGE_CATEGORY_BUTTON, SET_PLAYER_BUTTON}