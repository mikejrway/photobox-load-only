var path = require('path');
var webpack = require('webpack');
var ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: [
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'dist/public'),
        filename: 'bundle.[hash].js',
        publicPath: '/',
    },
    module: {
        rules: [{
            test: /\.js$/,
            include: [path.join(__dirname, 'src')],
            loader: 'babel-loader',
                options: {
                    presets: ['es2015']
                }
            },
            {test: /\.css$/, loader: ExtractTextWebpackPlugin.extract({fallback: "style-loader", use: "css-loader"})},
            {test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000'}
        ]        
    },
    plugins: [
        new ExtractTextWebpackPlugin("css/[name].[hash].css"),
        new HtmlWebpackPlugin({
            title: "Welcome to Question Time",
            template: "index.html",
            inject: "body"
        })
    ]
};
