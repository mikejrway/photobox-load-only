import Question from './Question';

class MultiAnswerQuestion extends Question {
    constructor(questionText, correctIndexes, ...answers) {
        super(questionText, -1, ...answers);
        this._correctIndexes = correctIndexes;
    }

    checkAnswer(answer) {
        return this._correctIndexes.sort().join() == answer.sort().join();
    }
}

export default MultiAnswerQuestion;