function categoryDetails(categories, selectedCategory) {
    return `<h4>Category: <select name='category'>
        ${categories.map(item => `<option value='${item}' ${item == selectedCategory ? 'selected' : ''}>${item}</option>`).join('')}
    </select></h4>`;
}

export default categoryDetails;