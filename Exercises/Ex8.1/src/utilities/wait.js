function wait(ms) {
    let prom = new Promise((resolve, reject) => {
        window.setTimeout(resolve, ms);
    });
    return prom;
}
export default wait;