class Question {
    constructor(questionText, correctIndex, ...answers) {
        this._questionText = questionText;
        this._correctIndex = correctIndex;
        this._answers = answers;
    }

    get questionText() {
        return this._questionText;
    }
    get correctIndex() {
        return this._correctIndex;
    }
    get answers() {
        return this._answers;
    }

    checkAnswer(answer) {
        return this._correctIndex == answer;
    }
}

export default Question;