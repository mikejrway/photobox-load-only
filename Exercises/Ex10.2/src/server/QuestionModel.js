import AWS from "aws-sdk";
import {getQuestionsForCategoryByScan} from "../dynamodb/dynamo-query";
import configureAWS from "../dynamodb/config";


export default async function (category) {
    configureAWS();
    let dynamodb = new AWS.DynamoDB();

    //let data = await getQuestionsForCategory(category);
    let data = await getQuestionsForCategoryByScan(category);
    data.Items.forEach(function (item) {
        console.log(" -", item.category + ": " + item.questionText);
    });
    return data;
};
