import AWS from "aws-sdk";
import fs from 'fs';



async function importData() {
    // An array of promises so we can wait for them all to finish
    let promises = [];

    console.log("Importing questions into DynamoDB. Please wait.");
    let docClient = new AWS.DynamoDB.DocumentClient();

    // Load all of the questions from the JSON file
    let allQuestions = JSON.parse(fs.readFileSync('data/questions.json', 'utf8'));
    // For each question, create a set of parameters to push to the data store
    allQuestions.forEach(function (question) {
        var params = {
            TableName: "Questions",
            Item: {
                "category": question.category,
                "questionText": question.questionText,
                "correctIndex": question.correctIndex,
                "answers": question.answers
            }
        };
        // Create a new promise for each question
        promises.push( new Promise((resolve, reject) => {
            docClient.put(params,  (err, data) => {
                if (err) {
                    let message = "Unable to add question ${question.questionText}. Error JSON: ${JSON.stringify(err, null, 2)}";
                    console.error(message);
                    reject(message);
                } else {
                    console.log("PutItem succeeded:", question.questionText);
                    resolve("PutItem succeeded: ${question.questionText}");
                }
            });
        }));
    });
    return Promise.all(promises);

}

export default importData;