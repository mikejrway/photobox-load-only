import AWS from "aws-sdk";

async function createTable() {
    let p = new Promise((resolve, reject) => {
        
        let dynamodb = new AWS.DynamoDB();

        let questionParams = {
            TableName: "Questions",
            KeySchema: [
                { AttributeName: "category", KeyType: "HASH" },  //Partition key
                { AttributeName: "questionText", KeyType: "RANGE" }  //Sort key
            ],
            AttributeDefinitions: [
                { AttributeName: "category", AttributeType: "S" },
                { AttributeName: "questionText", AttributeType: "S" }
            ],
            ProvisionedThroughput: {
                ReadCapacityUnits: 10,
                WriteCapacityUnits: 10
            }
        };



        dynamodb.createTable(questionParams,  (err, data) => {
            if (err) {
                let message = "Unable to create table. Error JSON:" + JSON.stringify(err, null, 2);
                console.error(message );
                reject(message);
            } else {
                let message = "Created table. Table description JSON:" + JSON.stringify(data, null, 2);
                console.log(message);
                resolve(message);
            }
        });
    });

    return p;
}

export default createTable;