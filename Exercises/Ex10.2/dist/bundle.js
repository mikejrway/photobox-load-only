/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("aws-sdk");

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_aws_sdk__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dynamo_query__ = __webpack_require__(3);
let runQuery = (() => {
    var _ref = _asyncToGenerator(function* () {
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */])();
        try {
            let questions = yield __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__dynamo_query__["a" /* getQuestionsForCategory */])("Easy");
            console.log(JSON.stringify(questions));
        } catch (e) {
            console.log("Error creating tables. Ignoring and attempting import");
        }
    });

    return function runQuery() {
        return _ref.apply(this, arguments);
    };
})();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }





runQuery().catch(() => {});

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_aws_sdk__);


function configureAWS() {
    var creds = new __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.Credentials('akid', 'secret', 'session');

    __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.config.update({
        region: "us-west-2",
        endpoint: "http://localhost:8000",
        credentials: creds
    });
}

/* harmony default export */ __webpack_exports__["a"] = (configureAWS);

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getQuestionsForCategory; });
/* unused harmony export getQuestionsForCategoryByScan */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_aws_sdk__);


function getQuestionsForCategory(cat) {

    let prom = new Promise((resolved, rejected) => {

        let docClient = new __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.DynamoDB.DocumentClient();

        console.log(`Querying for Questions in category = ${cat}`);

        let params = {
            TableName: "Questions",
            KeyConditionExpression: "#cat = :theCat",
            ExpressionAttributeNames: {
                "#cat": "category"
            },
            ExpressionAttributeValues: {
                ":theCat": cat
            }
        };

        docClient.query(params, function (err, data) {
            if (err) {
                console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
                rejected(err);
            } else {
                console.log("Query succeeded.");
                resolved(data);
            }
        });
    });
    return prom;
}

function getQuestionsForCategoryByScan(cat) {
    let prom = new Promise((resolved, rejected) => {

        let docClient = new __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.DynamoDB.DocumentClient();
        var params = {
            TableName: "Questions",
            FilterExpression: "#cat = :theCat",
            ExpressionAttributeNames: {
                "#cat": "category"
            },
            ExpressionAttributeValues: {
                ":theCat": cat
            }
        };

        docClient.scan(params, function (err, data) {
            if (err) {
                console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
                rejected(err);
            } else {
                console.log("Query succeeded.");
                resolved(data);
            }
        });
    });
    return prom;
}



/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ })
/******/ ]);
//# sourceMappingURL=bundle.js.map