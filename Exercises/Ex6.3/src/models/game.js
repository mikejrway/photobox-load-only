import Question from './Question';

let _score = 0, _total = 0,
    _question = new Question('Which ES version introduced properties?', 1, 'ES3', 'ES5', 'ES6');
const PLAYER_NAME = 'Your Name';

let game = {
    get score() {
        return _score;
    },
    get total() {
        return _total;
    },
    get playerName() {
        return PLAYER_NAME;
    },
    get question() {
        return _question;
    },
    checkAnswer (answer) {
        if (_question.checkAnswer(answer)) {
            _score++;
        }
        _total++;
    }
};

export default game;