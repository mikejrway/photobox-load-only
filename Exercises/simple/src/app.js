var Koa = require("koa");
var handlebars = require("koa-handlebars");

var app = new Koa();

// app.use(require("koa-favi")());
// app.use(require("koa-logger")());

app.use(handlebars({
  cache: app.env !== "development",
  defaultLayout: "main"
}));

app.use(async (ctx,next) => {
    console.log(JSON.stringify(app));
    await next();
});

app.use(async (ctx) => {
  await ctx.render("test", {
    user: { name: "World" }
  });
});

app.listen(3000);
