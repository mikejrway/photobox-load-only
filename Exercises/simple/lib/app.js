function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var Koa = require("koa");
var handlebars = require("koa-handlebars");

var app = new Koa();

// app.use(require("koa-favi")());
// app.use(require("koa-logger")());

app.use(handlebars({
  cache: app.env !== "development",
  defaultLayout: "main"
}));

app.use((() => {
  var _ref = _asyncToGenerator(function* (ctx, next) {
    console.log(JSON.stringify(app));
    yield next();
  });

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
})());

app.use((() => {
  var _ref2 = _asyncToGenerator(function* (ctx) {
    yield ctx.render("test", {
      user: { name: "World" }
    });
  });

  return function (_x3) {
    return _ref2.apply(this, arguments);
  };
})());

app.listen(3000);