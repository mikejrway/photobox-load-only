import * as UI from './UI';

export default function gameDetails(game) {
    let {playerName, score, total} = game;
    return `<h1>Welcome to Question Time ${playerName}</h1>

${UI.scoreUI(score, total)}

${UI.questionUI('What is the new var?', 'let', 'int', 'string', 'double')}
`;
}
