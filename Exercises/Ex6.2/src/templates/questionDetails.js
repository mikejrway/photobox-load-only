function questionDetails(questionText, ...answers) {
    return `<h3>${questionText}</h3>

    <ul>
      ${answers.map((item, index) => `<li><input type='radio' name='answer' value='${index}'> ${item}</li>`).join('')}
    </ul>
    `
}

export default questionDetails;