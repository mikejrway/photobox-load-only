function wait(ms = 5000) {
    let prom = new Promise((resolve, reject) => {
        window.setTimeout(resolve, ms);
    });

    return prom;
}

export default wait;