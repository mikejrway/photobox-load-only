import AWS from "aws-sdk";
import configureAWS from "./config";
import {getQuestionsForCategory} from "./dynamo-query";


async function runQuery(){
    configureAWS();
    try {
        let questions = await getQuestionsForCategory("Easy");
        console.log(JSON.stringify(questions));
    } catch (e) {
        console.log("Error creating tables. Ignoring and attempting import");
    }
}

runQuery().catch(()=>{});



