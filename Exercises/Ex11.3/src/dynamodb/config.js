import AWS from "aws-sdk";

function configureAWS() {
    var creds = new AWS.Credentials('akid', 'secret', 'session');

    AWS.config.update({
        region: "us-west-2",
        endpoint: "http://localhost:8000",
        credentials: creds
    });
}

export default configureAWS;
