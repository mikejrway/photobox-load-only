import AWS from "aws-sdk";
import configureAWS from "./config";
import createTable from "./create-table";
import importData from "./dynamo-import";


async function runImport(){
    configureAWS();
    try {
        await createTable();
    } catch (e) {
        console.log("Error creating tables. Ignoring and attempting import");
    }
    console.log("Import...");
    await importData();
    console.log("Import Done");
}

runImport().catch(()=>{});



