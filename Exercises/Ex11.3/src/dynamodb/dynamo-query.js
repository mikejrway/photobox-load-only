import AWS from "aws-sdk";


function getQuestionsForCategory(cat) {

    let prom = new Promise((resolved, rejected) => {

        let docClient = new AWS.DynamoDB.DocumentClient();

        console.log(`Querying for Questions in category = ${cat}`);

        let params = {
            TableName: "Questions",
            KeyConditionExpression: "#cat = :theCat",
            ExpressionAttributeNames: {
                "#cat": "category"
            },
            ExpressionAttributeValues: {
                ":theCat": cat
            }
        };


        docClient.query(params, (err, data) => {
            if (err) {
                console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
                rejected(err);
            } else {
                console.log("Query succeeded.");
                resolved(data);
            }
        });
    });
    return prom;
}


export default getQuestionsForCategory;