import Koa from "koa";
import handlebars from "koa-handlebars";
import router from "koa-route";


import questionBank from '../models/questionBank';

let _questions = [...questionBank.get('Easy')];

var app = new Koa();

app.use(handlebars({
  defaultLayout: "main"
}));


app.use(router.get('/questions', async (ctx, next) => {
  ctx.body=JSON.stringify(_questions);
  next(); 	
}));


app.use(async (ctx) => {
  await ctx.render("home", {
    title: "Test Page",
    playerName: "Fred Flintstone",
    scores: {
  	  score: "5",
      total: "10",
      percent: "50%"
    }
  });
});

app.use(ctx => {
  ctx.body = 'Hello World';
});

app.listen(3000);