/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("aws-sdk");

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_koa__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_koa___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_koa__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_koa_handlebars__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_koa_handlebars___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_koa_handlebars__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_koa_route__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_koa_route___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_koa_route__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__QuestionModel__ = __webpack_require__(4);
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }





 /////

// import questionBank from '../models/questionBank';

// let _questions = [...questionBank.get('Easy')];

var app = new __WEBPACK_IMPORTED_MODULE_0_koa___default.a();

app.use(__WEBPACK_IMPORTED_MODULE_1_koa_handlebars___default()({
  defaultLayout: "main"
}));

app.use(__WEBPACK_IMPORTED_MODULE_2_koa_route___default.a.get('/questions', (() => {
  var _ref = _asyncToGenerator(function* (ctx, next) {
    ctx.body = JSON.stringify((yield __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__QuestionModel__["a" /* default */])("Easy")));
    next();
  });

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
})()));

app.use((() => {
  var _ref2 = _asyncToGenerator(function* (ctx) {
    yield ctx.render("home", {
      title: "Test Page",
      playerName: "Fred Flintstone",
      scores: {
        score: "5",
        total: "10",
        percent: "50%"
      }
    });
  });

  return function (_x3) {
    return _ref2.apply(this, arguments);
  };
})());

app.use(ctx => {
  ctx.body = 'Hello World';
});

app.listen(3000);

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_aws_sdk__);


function configureAWS() {
    var creds = new __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.Credentials('akid', 'secret', 'session');

    __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.config.update({
        region: "us-west-2",
        endpoint: "http://localhost:8000",
        credentials: creds
    });
}

/* harmony default export */ __webpack_exports__["a"] = (configureAWS);

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_aws_sdk__);


function getQuestionsForCategory(cat) {

    let prom = new Promise((resolved, rejected) => {

        let docClient = new __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default.a.DynamoDB.DocumentClient();

        console.log(`Querying for Questions in category = ${cat}`);

        let params = {
            TableName: "Questions",
            KeyConditionExpression: "#cat = :theCat",
            ExpressionAttributeNames: {
                "#cat": "category"
            },
            ExpressionAttributeValues: {
                ":theCat": cat
            }
        };

        docClient.query(params, (err, data) => {
            if (err) {
                console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
                rejected(err);
            } else {
                console.log("Query succeeded.");
                resolved(data);
            }
        });
    });
    return prom;
}

/* harmony default export */ __webpack_exports__["a"] = (getQuestionsForCategory);

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_aws_sdk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_aws_sdk__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dynamodb_dynamo_query__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dynamodb_config__ = __webpack_require__(2);
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }





/* harmony default export */ __webpack_exports__["a"] = ((() => {
    var _ref = _asyncToGenerator(function* (category) {
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__dynamodb_config__["a" /* default */])();
        return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__dynamodb_dynamo_query__["a" /* default */])(category);
    });

    return function (_x) {
        return _ref.apply(this, arguments);
    };
})());

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("koa");

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("koa-handlebars");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("koa-route");

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ })
/******/ ]);
//# sourceMappingURL=server-bundle.js.map