import * as UI from './UI';
import MultiAnswerQuestion from '../models/MultiAnswerQuestion';

function gameDetails(game) {
    let {playerName, score, total, question, categories, selectedCategory} = game;
    let {questionText, answers} = question;
    let type = question instanceof MultiAnswerQuestion ? 'checkbox' : 'radio';

    return `<h1>Welcome to Question Time ${playerName}</h1>

${UI.categoryUI(categories, selectedCategory)}

${UI.scoreUI(score, total)}

${game.over ? '<h2>Game Over</h2>' : UI.questionUI(questionText, type, ...answers)}
`;
}

export default gameDetails;