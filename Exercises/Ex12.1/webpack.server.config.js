var webpack = require('webpack');
const path = require('path');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function(x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

module.exports = {
    devtool: 'source-map',
    entry: [
        './src/server/main-koa'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'backend.js',
        publicPath: '/'
    },
    module: {
        rules: [{
            test: /\.js$/,
            include: [path.join(__dirname, 'src')],
            loader: 'babel-loader',
            options: {
                presets: ['es2015'],
                plugins: ['transform-async-to-generator', 'babel-plugin-transform-runtime']
            }
        }]
    },
    externals: nodeModules,
    target: 'node'
}