import Koa from "koa";
import handlebars from "koa-handlebars";
import router from "koa-route";

import getQuestions from "./QuestionModel"; 

var app = new Koa();



app.use(router.get('/questions', async (ctx, next) => {
  ctx.body=JSON.stringify(await getQuestions("Easy"));
  next(); 	
}));

app.listen(3000);