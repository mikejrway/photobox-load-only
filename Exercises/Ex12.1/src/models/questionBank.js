import Question from './Question';
import MultiAnswerQuestion from './MultiAnswerQuestion';
import getData from '../utilities/getData';

let _questionSet = new Set([
    new Question('Which version of ES added get and set properties?', 1, 'ES3', 'ES5', 'ES6'),
    new Question('What is the name of the new function syntax?', 2, 'dagger', 'spear', 'arrow', 'shotgun'),
    new Question('Which of the following function definitions includes the rest operator?', 3, 'function () { }', 'function (a) { }', 'function (a = 1) { }', 'function (...a) { }'),
    new MultiAnswerQuestion('Which parameter(s) has a/have default value(s): function (a = 0, b, c = 1, d) { }?', [0, 2], 'a', 'b', 'c', 'd')
]);

let _mediumQuestionSet = new Set([
    new Question('What would be displayed? console.log(Number.isNaN("abc"))', 1, 'true', 'false', '"abc"', 'null', 'undefined'),
    new Question('What would be displayed? let a = NaN; console.log(Number.isNaN(a))', 0, 'true', 'false', '"abc"', 'null', 'undefined'),
    new Question('What would be displayed? let a = 2.0; console.log(Number.isNaN(a))', 1, 'true', 'false', '"abc"', 'null', 'undefined'),
    new MultiAnswerQuestion('What would be displayed? console.log([1, 2, 3].map((i) => i.toString()).join(""))', [0], '123', '1 2 3', '1,2,3', '[1,2,3]')
]);

let _questionMap = new Map([
    ['Easy', _questionSet],
    ['Medium', _mediumQuestionSet],
]);

getData('http://api.questiontime.com/questions') 
    .then(JSON.parse)
    .then(data => {
        data.forEach(item => {
            const {questionText, category, correctIndex, answers} = item;
            const ques = new Question(questionText, correctIndex, ...answers);
            _questionMap.get(category).add(ques);
        });
    });

export default _questionMap;