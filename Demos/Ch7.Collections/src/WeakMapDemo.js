const _fields = new WeakMap();

class Person {
    constructor(firstName, lastName) {
        _fields.set(this, {firstName, lastName});
    }
    get firstName() {
        return _fields.get(this).firstName;
    }
    get lastName() {
        return _fields.get(this).lastName;
    }
}

export {Person};