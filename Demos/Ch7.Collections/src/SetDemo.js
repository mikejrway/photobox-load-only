const primes = new Set([2, 3, 5, 7, 7]);

console.log(primes.size);

console.log(primes.has(11));

console.log(primes.add(11));
console.log(primes.add(11));

console.log(primes.delete(7));
console.log(primes.delete(7));
primes.clear();