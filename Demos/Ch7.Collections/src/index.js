'use strict';
import 'babel-polyfill';
import * as nb from './nb';

//import {Person} from './WeakMapDemo';
//
//let lora = new Person('Lora', 'Greatfield');
//
//console.log(`First Name: ${lora.firstName}, Last Name: ${lora.lastName}`);
//
//lora.firstName = 'Greg';

let eventSource = {};

nb.addListener(eventSource, () => {
   console.log('Event was raised');
});

nb.notify(eventSource);