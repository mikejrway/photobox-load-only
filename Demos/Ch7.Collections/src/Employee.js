import Person from './Person';

class Employee extends Person {

    constructor(firstName, lastName, dateOfBirth, jobTitle, startDate) {
        super(firstName, lastName, dateOfBirth);
        this._jobTitle = jobTitle
        this._startDate = startDate;
    }


    logFullName() {
        console.log(this.fullName + ` started work on ${this._startDate} as with a job title of ${this._jobTitle}`);
    }
}

//function Employee(firstName, lastName, dateOfBirth, jobTitle, startDate) {
//    Person.call(this, firstName, lastName, dateOfBirth);
//    this._jobTitle = jobTitle;
//    this._startDate = startDate;
//}
//
//Employee.prototype = Object.create(Person.prototype);
//Employee.prototype.constructor = Employee;
//Employee.prototype.logFullName = function() {
//    console.log(this.fullName + ` started work on ${this._startDate} as with a job title of ${this._jobTitle}`);
//}

export default Employee;