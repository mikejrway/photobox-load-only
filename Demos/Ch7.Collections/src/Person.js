class Person {

    constructor(firstName, lastName, dateOfBirth) {
            this._firstName = firstName;
            this._lastName = lastName;
            this._dateOfBirth = dateOfBirth;
    }


    get fullName() {
        return `${this._firstName} ${this._lastName} (${this.age})`;
    }

    get age() {
        const today = new Date();
        const birthDate = this._dateOfBirth;
        let age = today.getFullYear() - birthDate.getFullYear();
        const m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    logFullName() {
        console.log(this.fullName);
    }
}

export default Person;