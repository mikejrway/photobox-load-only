//let dict = Object.create(null);
//
//dict['evens'] = [0, 2, 4, 6, 8];
//dict['odds'] = [1, 3, 5, 7, 9];
//dict['primes'] = [2, 3, 5, 7, 11];
//
//for (var key in dict) {
//    console.log(key);
//    dict[key].forEach(function (item) {
//        console.log(item);
//    });
//    console.log('');
//}



let nums = new Map([
    ['evens',[0, 2, 4, 6, 8]],
    ['odds',[1, 3, 5, 7, 9]],
]);

nums.set('primes', [2, 3, 5, 7, 11]);

console.log(nums.size);

let removed = nums.delete('odds');
console.log(removed);

//nums.clear();


let keys = nums.keys();
let values = nums.values();
let entries = nums.entries();

console.log('keys');
Array.from(keys).forEach((item) => {
    console.log(item);
})
console.log('');

console.log('values');
Array.from(values).forEach((item) => {
    console.log(item);
})
console.log('');

console.log('entries');
Array.from(entries).forEach((item) => {
    let [key, value] = item;
    console.log(key);
    console.log(value);
})
console.log('');




export default nums;