let _callbacks = new WeakMap();


function addListener(sourceObject, callback) {
    if (_callbacks.has(sourceObject)) {
        _callbacks.get(sourceObject).add(callback);
    } else {
        _callbacks.set(sourceObject, new Set([callback]));
    }
}

function notify(sourceObject) {
    const callbacks = _callbacks.get(sourceObject);
    if (!callbacks) return;
    for (const callback of callbacks) {
        callback();
    }
}

export {addListener, notify}