const arrayObject = [2, 3, 5, 7, 11];

for (const item of arrayObject) {
    if (item < 4) continue;
    if (item > 7) break;
    console.log(item);
}

for (const [index, value] of arrayObject.entries()) {
    console.log(`Index:${index}, Value:${value}`);
}



let callbacks = [];

console.log('Bad: Don\'t use var');
for (var item of arrayObject) {
    callbacks.push(() => {console.log(`bad: ${item}`)});
}
callbacks.forEach(item => item());


console.log('Good: Do use const');
for (const element of arrayObject) {
    callbacks.push(() => {console.log(`good: ${element}`)});
}

callbacks.forEach(item => item());


let nums = new Map([
    ['evens',[0, 2, 4, 6, 8]],
    ['odds',[1, 3, 5, 7, 9]],
    ['primes',[2, 3, 5, 7, 11]],
]);


for (const [key, value] of nums) {
    console.log(`Key:'${key}', Value:${value}`);
}