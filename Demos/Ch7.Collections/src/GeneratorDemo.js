function* evens() {
    let i = 0;
    while (true) {
        yield i += 2;
    }
}
//
//for (const value of evens()) {
//    if (value < 20) continue;
//    if (value > 100) break;
//    console.log(value);
//}

const iterator = evens();

while (true) {
    const {done, value} = iterator.next();

    console.log(`Done: ${done}, Value: ${value}`)

    if (value > 100) {
        break;
    }
}



function* fibonacci() {
    let current = 0;
    let next = 1;
    while (true) {
        const temp = current + next;

        yield current;

        current = next;
        next = temp;
    }
}

const sequences = new Map([
    ['evens', evens],
    ['fibonacci', fibonacci],
]);
//
//for (const [key, series] of sequences) {
//    console.log(key);
//    for (const value of series()) {
//        if (value > 100) break;
//        console.log(value);
//    }
//    console.log('');
//}