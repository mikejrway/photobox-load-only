const primes = new Set([2, 3, 5, 7, 7]);

const pIterator = primes.values();

const firstEntry = pIterator.next();

console.log(firstEntry.done);
console.log(firstEntry.value);

let entry = pIterator.next();
while (!entry.done) {
    console.log(entry.done);
    console.log(entry.value);
    entry = pIterator.next();
}
