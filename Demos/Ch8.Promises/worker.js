onmessage = function (e) {
    var number1 = e.data[0];
    var number2 = e.data[1];
    console.log('In worker: ' + number1 + ' * ' + number2);
    postMessage(number1 * number2);
}