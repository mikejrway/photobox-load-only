function later(ms = 1000) {
    let prom = new Promise((resolved, rejected) => {
        window.setTimeout(resolved, ms);
    });
    return prom
}

export default later;