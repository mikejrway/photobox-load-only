function maybe(ms = 5000, fractionPositive = 0.5) {
    let prom = new Promise((resolved, rejected) => {
        let callback;
        if (Math.random() < fractionPositive) {
            callback = resolved;
        } else {
            callback = rejected;
        }
        window.setTimeout(callback, ms);
    });
    return prom
}
export default maybe;