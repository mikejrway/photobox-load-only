// async function read () {
//   var html = await getRandomPonyFooArticle();
//   var md = hget(html, {
//     markdown: true,
//     root: 'main',
//     ignore: '.at-subscribe,.mm-comments,.de-sidebar'
//   });
//   var txt = marked(md, {
//     renderer: new Term()
//   });
//   return txt;
// }


// async function write () {
//   var txt = await read();
//   console.log(txt);
// }


//read().then(txt => console.log(txt));


// async function asyncFun () {
//   var value = await Promise
//     .resolve(1)
//     .then(x => x * 3)
//     .then(x => x + 5)
//     .then(x => x / 2);
//   return value;
// }

async function asyncAwaitTest(){
    return "42";
}

async function asyncAwaitPromiseTest(fail = false){
    let p = new Promise((resolve, reject)=>{
        if(fail){
            reject("error");
        }
        resolve("42");
    });
    return p;
}

async function asyncAwaitPromiseTest_Throw(fail = false){
    if(fail){
        throw "It failed";
    }
    return 42;
}


// asyncFun().then(x => console.log(`x: ${x}`));

// async function as_later(ms = 1000) {
//     let prom = new Promise((resolved, rejected) => {
//         window.setTimeout(resolved, ms);
//     });
//     return prom
// }

export  {asyncAwaitTest, asyncAwaitPromiseTest, asyncAwaitPromiseTest_Throw};