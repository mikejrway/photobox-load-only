function where() {
    let prom = new Promise((resolved, rejected) => {
        navigator.geolocation.getCurrentPosition((pos) => {
            resolved(pos);
        }, (err) => {
            rejected(err);
        });
    });
    return prom
}

export default where;