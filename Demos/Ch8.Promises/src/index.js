'use strict';
import 'babel-polyfill';


import where from './where';

where().then((pos) => {
   console.log('Yes, succeeded:');
   console.log(pos)
}, (err) => {
   console.log('No, failed:');
   console.log(err);
});

import {asyncAwaitTest, asyncAwaitPromiseTest, asyncAwaitPromiseTest_Throw} from './async_await';

//import './asynchronousPreES2015';

//import './asyncNoPromise';

//import later from './later';
//
//console.log('before async');
//later(2000)
//    .then(() => { console.log('first'); })
//    .then(later)
//    .then(() => { console.log('second'); return 5000; })
//    .then(later)
//    .then(() => { console.log('third'); return 400; })
//    .then(later)
//    .then(() => { console.log('fourth'); })
//    .then(later);
//console.log('after async');

// let pOne = later(5000).then(() => 'one');
// let pTwo = later(1000).then(() => 'two');

// Promise.all([pOne, pTwo])
//     .then(([rOne, rTwo]) => {
//    console.log(rOne);
//    console.log(rTwo);
// });

// Promise.race([pOne, pTwo])
//    .then(first => {
//       console.log(first);
//    });

document.getElementById('clicker')
    .addEventListener('click', (e) => {
      const target = document.getElementById('target');
    //   asyncAwaitTest().then((result) => {
        
    //       console.log("Result:", result);
    //       target.textContent = result;
    //    });
    //   let result = asyncAwaitTest();
    //   console.log("Result:", result);
    //getErrorPromise();
    getError();
   
    });


async function getResult() {
    let result  = await asyncAwaitTest();
    console.log("Result:", result);
}

async function getError() {
    try {
        //let result  = await asyncAwaitPromiseTest(true);
        let result = await asyncAwaitPromiseTest_Throw(true);
        console.log("Result:", result);
    } catch (err) {
        console.log("Error:", err);
    }
}

function getErrorPromise() {

    asyncAwaitPromiseTest(true).then(result => {
    console.log("Result:", result);
    }).catch(err => console.log("Error:", err));

}


//import maybe from './maybe';
//
//
//maybe(1000, 0.8)
//    .then(()=>{console.log('first resolved')})
//    .then(() => maybe(1000, 0.8))
//    .then(()=>{console.log('second resolved')})
//    .then(() => maybe(1000, 0.8))
//    .then(()=>{console.log('third resolved')})
//    .then(() => maybe(1000, 0.8))
//    .then(()=>{console.log('fourth resolved')})
//    .then(() => maybe(1000, 0.8))
//    .then(()=>{console.log('fifth resolved')})
//    .catch(() => {console.log('rejected at the end')});
//
//maybe(1000, 0.8)
//    .then(()=>{console.log('first resolved')})
//    .then(() => maybe(1000, 0.8))
//    .then(()=>{console.log('second resolved')}, ()=>{console.log('second rejected')})
//    .then(() => maybe(1000, 0.8))
//    .then(()=>{console.log('third resolved')})
//    .then(() => maybe(1000, 0.8))
//    .then(()=>{console.log('fourth resolved')}, ()=>{console.log('fourth rejected')})
//    .then(() => maybe(1000, 0.8))
//    .then(()=>{console.log('fifth resolved')})
//    .catch(() => {console.log('rejected at the end')});