console.log('Before async');
setTimeout(function () {
    console.log('I happen later');
}, 1000);
console.log('After async');

navigator.geolocation.getCurrentPosition(function (coords) {
   console.log(coords);
});

var ww = new Worker('worker.js');

setInterval(function () {
    var number1 = Math.round(Math.random() * 100);
    var number2 = Math.round(Math.random() * 100);
    ww.postMessage([number1, number2]);
}, 1000);

ww.onmessage = function (e) {
    console.log('Result: ' + e.data);
};