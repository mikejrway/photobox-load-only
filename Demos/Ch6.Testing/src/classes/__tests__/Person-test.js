jest.autoMockOff();
const Person = require('../Person').default;

describe('The Person class', () => {

    let lora, rory;
    beforeEach(() => {
        lora = new Person('Lora', 'Greatfield');
        rory = new Person('Rory', 'O\'Malley');
    });

    it('should have firstName property', () => {
        expect(lora.firstName).toEqual('Lora');
    });
    it('should have lastName property', () => {
        expect(lora.lastName).toEqual('Greatfield');
    });
    it ('should have a fullName property', () => {
        expect(lora.fullName).toEqual('Lora Greatfield');
    });
    it ('should greet another person', () => {
       expect(lora.greet(rory)).toEqual('Lora Greatfield says bye to Rory O\'Malley');
    });
});