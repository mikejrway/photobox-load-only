export default class Person {
    constructor(firstName, lastName) {
        this._firstName = firstName;
        this._lastName = lastName;
    }

    get firstName() {
        return this._firstName;
    }

    get lastName() {
        return this._lastName;
    }

    get fullName() {
        return `${this._firstName} ${this._lastName}`;
    }

    greet(other) {
        return `${this.fullName} says hi to ${other.fullName}`;
    }
}