
// find the roots of a * x * x + b * x + c
function quadraticRoots(a = 1, b = -5, c = 6, iOK = true) {
    // function body omitted
    let determinant;
    determinant = b * b - 4 * a * c;
    if (determinant > 0) {
        const r1 = (-b + Math.sqrt(determinant)) / (2 * a);
        const r2 = (-b - Math.sqrt(determinant)) / (2 * a);
        return [r1, r2];
    }
    else if (determinant == 0) {
        const r1 = -b / (2 * a);
        return [r1];
    }
    else {
        if (!iOK) return 'Setting doesn\'t allow imaginary result';
        const real = -b / (2 * a);
        const imag = Math.sqrt(-determinant) / (2 * a);
        return [{real, imag}, {real, imag:-imag}];
    }
}



export default quadraticRoots;