jest.autoMockOff();

const quadraticRoots = require('../quadraticRoots').default;

describe('The quadraticRoots function', () => {
    it('when called without arguments should return [3, 2]', () => {
        expect(quadraticRoots()).toEqual([3, 2]);
    });
});