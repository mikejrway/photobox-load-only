jest.autoMockOff();

const lora = require('../lora').default;

describe('The lora object', () => {
    it('should have firstName property', () => {
        expect(lora.firstName).toEqual('Lora');
    });
    it('should have lastName property', () => {
        expect(lora.lastName).toEqual('Greatfield');
    });
    it ('should have a fullName property', () => {
        expect(lora.fullName).toEqual('Lora Greatfield');
    });
});