let _firstName = 'Lora',
    _lastName = 'Greatfield'

export default {
    get firstName() {
        return _firstName;
    },
    get lastName() {
        return _lastName;
    },
    get fullName() {
        return `${_firstName} ${_lastName}`
    }

};