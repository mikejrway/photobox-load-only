// Allocate some memory and assign to var1
var var1 = {
    'value' : 1
}

// Reference count is now 1
var var2 = var1;

// Reference count is now 2

var2 = null;

// Reference count is now 1

var1=null;

// Reference count is now 0
// Memory available for garbage collection
