'use strict';

//var welcomeMessage = 'Hello';
//var hourOfDay = new Date(Date.now()).getHours();
//var isMorning = hourOfDay < 12;
//
////isThisOK = false;
//
//console.log(welcomeMessage);
//console.log(hourOfDay);
//console.log(isMorning);
//
//console.log('$' + calculatePay(65, 50, 40, 80));
//
//function calculatePay(hoursWorked, standardRate, standardHours, overtimeRate) {
//    var standardTotal = hoursWorked * standardRate;
//    var overtimeTotal = (hoursWorked - standardHours) * overtimeRate;
//    return standardTotal + overtimeTotal;
//}
//
//for (var i = 0 ; i < 10 ; i++) {
//    console.log(i);
//}
//console.log('After:' + i);
//
//
//for (let j = 0 ; j < 10 ; j++) {
//    console.log(j);
//}
//console.log('After:' + j);
//
//const MAX = 100;
////MAX = 101;
//
//const primes = [2, 3, 5, 7];
//primes.push(11);
//console.log(primes);
////primes = [13, 17, 19];


//{
//    var i = 10;
//    console.log('Inside block:' + i);
//}
//console.log('Outside block:' + i);
//
//
//(function() {
//    var i = 10;
//    console.log('Inside IIFE:' + i);
//})();
//console.log('Outside IIFE:' + i);


//console.log(0o16);
//console.log(0b01110);
//
//console.log(Number.isNaN('xxx'));
//console.log(Number.isNaN(NaN));
//console.log(isNaN('xxx'));
//
//console.log(Number.isInteger(123.1));
//console.log(Number.isInteger(123));


//let stringVariable = 'Hello World';
//console.log(stringVariable.startsWith('He'));
//console.log(stringVariable.endsWith('ld'));
//console.log(stringVariable.includes('or'));
//console.log(stringVariable.startsWith('He', 1));
//console.log(stringVariable.startsWith('ell', 1));

let someString = `template content
can span multiple lines, include both
'single' and "double" quotes without escaping`;

console.log(someString);
console.log(typeof someString);

function getAge(date) {
    var today = new Date();
    var birthDate = new Date(date);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

let firstName = 'Lora'; let birthDate = new Date('1990-01-01');
let welcomeMessage = `Welcome ${firstName}.
You are ${getAge(birthDate)} years old.`;

console.log(welcomeMessage);
console.log(typeof someString);


