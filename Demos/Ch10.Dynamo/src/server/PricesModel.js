import AWS from "aws-sdk";
import {scanByCat, queryByCat} from "../dynamodb/dynamo-query";
import configureAWS from "../dynamodb/config";


export default async function (category) {
    configureAWS();
    let dynamodb = new AWS.DynamoDB();

    let data = await queryByCat(category);
    //let data = await scanByCat(category);
    data.Items.forEach(function (item) {
        console.log(" -", item.category + ": " + item.questionText);
    });
    return data.Items;
};
