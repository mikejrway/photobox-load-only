import Koa from "koa";
import _ from "koa-route"

import getPrices from "./PricesModel";

var app = new Koa();

app.use(_.get('/prices', async (ctx, next) => {
    try{
        ctx.body=JSON.stringify(await getPrices("music"));
    } catch (e) {
        ctx.body = "It's not working. Check the server-side logs and make sure that DynamoDB is running!"
    }
  next();
}));

app.listen(3000);

/// If you get the DynamoDB error: make sure you have run it!
/// java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb -inMemory
// From the course directory


