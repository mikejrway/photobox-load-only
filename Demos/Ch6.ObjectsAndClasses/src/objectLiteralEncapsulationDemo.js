// private data
let _firstName = 'Lora',
    _lastName = 'Greatfield',
    _dateOfBirth = new Date('1995-03-01');

let lora = {
    // accessors give access
    get firstName() { return _firstName },
    get lastName() { return _lastName },
    get dateOfBirth() { return _dateOfBirth },

    get fullName() { return `${this.firstName} ${this.lastName}`; },

    greet(other) {
        console.log(`${this.fullName} says hello to ${other.fullName}`);
    },
}

export  {lora};
