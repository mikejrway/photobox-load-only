import Person from './classDemo';

class Employee extends Person {

    constructor(firstName, lastName, dateOfBirth, jobTitle, startDate) {
        super(firstName, lastName, dateOfBirth);
        this._jobTitle = jobTitle
        this._startDate = startDate;
    }

    get fullName() {
        return super.fullName +  `, who started work on ${this._startDate} with a job title of ${this._jobTitle}`;
    }

    greet(otherPerson) {
        console.log(`${this.fullName}, says how's things to ${otherPerson.fullName}`);
    }
}

//function Employee(firstName, lastName, jobTitle) {
//    Person.call(this, firstName, lastName);
//    this._jobTitle = jobTitle;
//}
//
//Employee.prototype = Object.create(Person.prototype);
//Employee.prototype.constructor = Employee;
//Employee.prototype.greet = function(other) {
//    console.log(Person.greet.call(this, other) +  this._fullName + ' works as a ' + this._jobTitle);
//}

export default Employee;