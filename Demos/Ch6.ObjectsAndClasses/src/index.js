'use strict';
//import {lora} from './objectLiteralDemo';
//import Person from './classDemo';
//import Employee from './inheritanceDemo';
//import './objectDestructuringDemo';
//import './simplifiedPropertiesDemo';

import Person from './symbolDemo';


let lora = new Person('Lora', 'Greatfield');
let rory = new Person('Rory', 'O\'Malley');

let _firstNameSymbol = Symbol();

lora[_firstNameSymbol] = 'Lauri';

for (const s of Object.getOwnPropertySymbols(lora)) {
    lora[s] = 'Ha ha';
}


lora.greet(rory);
rory.greet(lora);

//
//let rory = new Person('Rory', 'O\'Malley', new Date('1990-11-23'));
//
//lora.greet(rory);
//rory.greet(lora);
//
//
//let neil = new Employee('Neil', 'Armstrong', new Date('1970-01-01'), 'Author', new Date('2000-10-23'));
//neil.greet(rory);
//
//console.log('Is rory instanceof Person? ' + (rory instanceof Person));
//console.log('Is rory instanceof Employee? ' + (rory instanceof Employee));
//console.log('Is neil instanceof Person? ' +  (neil instanceof Person));
//console.log('Is neil instanceof Employee? ' +  (neil instanceof Employee));