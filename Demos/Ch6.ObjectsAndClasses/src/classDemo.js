class Person {

    constructor(firstName, lastName) {
        this._firstName = firstName;
        this._lastName = lastName;
    }

    get fullName() {
        return `${this._firstName} ${this._lastName}`;
    }

    greet(otherPerson) {
        console.log(`${this.fullName} says hello to ${otherPerson.fullName}`);
    }
}

var lora = new Person('Lora', 'Greatfield');
var rory = new Person('Rory', 'O\'Malley');

lora.greet(rory);
rory.greet(lora);

//
//function Person(firstName, lastName) {
//    this.firstName = firstName;
//    this.lastName = lastName;
//}
//
//Person.prototype.greet = function(other) {
//    console.log(this.fullName + ' says hello to ' + other.fullName);
//};


export default Person;