import {lora} from './objectLiteralDemo';

let {firstName, lastName, dateOfBirth} = lora;

let otherPerson = {firstName, lastName, dateOfBirth};

console.log('Property and value for otherPerson.firstName: ' + otherPerson.firstName);