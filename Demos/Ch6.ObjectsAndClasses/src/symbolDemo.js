const _firstNameSymbol = Symbol('firstName'),
    _lastNameSymbol = Symbol('lastName');

class Person {

    constructor(firstName, lastName) {
        this[_firstNameSymbol] = firstName;
        this[_lastNameSymbol] = lastName;
    }

    get fullName() {
        return `${this[_firstNameSymbol]} ${this[_lastNameSymbol]}`;
    }

    greet(otherPerson) {
        console.log(`${this.fullName} says hello to ${otherPerson.fullName}`);
    }
}

export default Person;