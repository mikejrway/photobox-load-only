// pre-ES5

//var lora = {
//    firstName: 'Lora',
//    lastName: 'Greatfield',
//    dateOfBirth: new Date('1995-03-01'),
//
//    greet: function(otherPerson) {
//        console.log(this.firstName + ' ' + this.lastName + ' says hello to ' + otherPerson.firstName + ' ' + otherPerson.lastName);
//    }
//}
//var rory = {
//    firstName: 'Rory',
//    lastName: 'O\'Malley',
//    dateOfBirth: new Date('1991-10-24'),
//
//    greet: function(otherPerson) {
//        console.log(this.firstName + ' ' + this.lastName + ' says hello to ' + otherPerson.firstName + ' ' + otherPerson.lastName);
//    }
//}
//lora.greet(rory);
//rory.greet(lora);

//ES5
//var lora = {
//    firstName: 'Lora',
//    lastName: 'Greatfield',
//    dateOfBirth: new Date('1995-03-01'),
//    get age() {
//        var today = new Date();
//        var birthDate = this.dateOfBirth;
//        var age = today.getFullYear() - birthDate.getFullYear();
//        var m = today.getMonth() - birthDate.getMonth();
//        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
//            age--;
//        }
//        return age;
//    },
//    get fullName() {
//        return this.firstName + ' ' + this.lastName + ' (' + this.age + ')';
//    },
//    set fullName(value) {
//      var splitNames = value.split(' ');
//        this.firstName = splitNames[0];
//        this.lastName = splitNames[1];
//    },
//    greet: function(otherPerson) {
//        console.log(this.fullName + ' says hello to ' + otherPerson.fullName);
//    }
//};
//
//lora.fullName = 'Lauri Wellgrazing';
//console.log(lora.firstName);
//console.log(lora.lastName);

var rory = {
    firstName: 'Rory',
    lastName: 'O\'Malley',
    dateOfBirth: new Date('1995-03-01'),
    get age() {
        var today = new Date();
        var birthDate = this.dateOfBirth;
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    },
    get fullName() {
        return this.firstName + ' ' + this.lastName + ' (' + this.age + ')';
    },

    greet: function(otherPerson) {
        console.log(this.fullName + ' says hello to ' + otherPerson.fullName);
    }
};


let lora = {
    firstName: 'Lora',
    lastName: 'Greatfield',
    dateOfBirth: new Date('1995-03-01'),

    get fullName() {
        return `${this.firstName} ${this.lastName} (${this.age})`;
    },

    get age() {
        const today = new Date();
        const birthDate = this.dateOfBirth;
        let age = today.getFullYear() - birthDate.getFullYear();
        const m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    },

    greet(otherPerson) {
        console.log(`${this.fullName} says hello to ${otherPerson.fullName}`);
    }
}

export  {lora};
