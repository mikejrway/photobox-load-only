'use strict';
import 'babel-polyfill';


import getData from './getData';

let questionPromise = getData('http://api.questiontime.com/questions');
questionPromise.then((data) => {
    console.log('Raw JSON data');
    console.log(data);
});
questionPromise
    .then(JSON.parse)
    .then((parsedData) => {
        console.log('Parsed JSON data');
        console.log(parsedData);
    })
    .catch((err) => {
        console.log('An error occurred');
        console.log(err);
    });