function getData(url) {
    let prom = new Promise((resolved, rejected) => {
        let req = new XMLHttpRequest();
        req.open('GET', url);
        req.addEventListener('load', () => {
            const {status, statusText, responseText} = req;
            if (status == '200') {
                resolved(responseText);
            } else {
                rejected({status, statusText, responseText});
            }
        });
        req.send();
    });
    return prom;
}

export default getData;