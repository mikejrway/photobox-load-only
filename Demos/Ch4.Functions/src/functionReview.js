// function statement
function hello(name) {
    return 'Hello ' + name;
}
// function expression
let welcome = function (name) {
    return 'Welcome ' + name;
}
// function constructor
let bonjour = new Function('nom', 'return "Bonjour " + nom')

// function invocations
console.log(hello('Lora'));
console.log(welcome('Rory'));
console.log(bonjour('Gerard'));

// callbacks, .bind(...) method, arguments
const links = document.querySelectorAll('.mathLink');
Array.prototype.slice.call(links).map(function (link) {
    link.addEventListener('click',
        (function clickHandler() {
            const number1 = arguments[0];
            const number2 = arguments[1];
            alert(number1 + number2);
        }).bind(link,
            Number.parseInt(link.dataset.number1),
            Number.parseInt(link.dataset.number2)));
});
