//function Person(name, age) {
//    console.log("In constructor: " + this); // prints object
//    this.name = name;
//    this.age = age;
//    this.log = function () {
//        console.log(this.name + ' is ' + this.age);
//    }
//    this.warpTime = function () {
//        console.log("In method: " + this); // prints object
//        setTimeout(function () {
//            console.log("In callback: " + this); // prints window
//        }, 1000);
//    }
//}

//function Person(name, age) {
//    this.name = name;
//    this.age = age;
//    this.log = function () {
//        console.log(this.name + ' is ' + this.age);
//    }
//    this.warpTime = function () {
//        setInterval(function () {
//            this.age++;
//            this.log();
//        }.bind(this), 1000);
//    }
//}


//var lora = new Person('Lora', 21);
//lora.warpTime();

//function bla() {
//    console.log("In regular function: " + this); // prints undefined
//}
//
//bla();

function Person(name, age) {
    this.name = name;
    this.age = age;
    this.log = function () {
        console.log(this.name + ' is ' + this.age);
    }
    this.warpTime = function () {
        setInterval(() => {
            this.age++;
            this.log();
        }, 1000);
    }
}

var lora = new Person('Lora', 21);
lora.warpTime();

