function multiplyAll(by, ...numbers) {
    //return numbers.map((item) => { return by * item; });
    //return numbers.map((item) => by * item);
    //return numbers.map(item => { return by * item; });
    return numbers.map(item => by * item);
}
function multiplyAllES5(by) {
    var numbers = Array.prototype.slice.call(arguments,
        multiplyAllES5.length,
        arguments.length);
    return numbers.map(function (item) {
        return by * item;
    });
}

const multiplyAllAFE = (by, ...numbers) => numbers.map(item => by * item);

// arrow function callback
console.log('ES2015 callback uses arrow function');
console.log(multiplyAll(3, 1, 2, 3));

// inline function callback
console.log('ES5 callback uses anonymous inline function');
console.log(multiplyAllES5(3, 1, 2, 3));

// Arrow function expression
console.log('ES2015 arrow function expression');
console.log(multiplyAllAFE(3, 1, 2, 3));
