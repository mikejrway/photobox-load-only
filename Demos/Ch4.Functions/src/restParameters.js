//function sum(...numbers) {
//    let total = 0;
//    numbers.forEach(function (item) {
//       total += item;
//    });
//    return total;
//}
//// no evidence of parameter values in function declaration
//function sumES5() {
//    var numbers = Array.prototype.slice.call(arguments);
//    var total = 0;
//    numbers.forEach(function (item) {
//        total += item;
//    });
//    return total;
//}
//
//function multiplySum(multiplyBy, ...numbers) {
//    let total = 0;
//    numbers.forEach(function (item) {
//        total += item;
//    });
//    return multiplyBy * total;
//}
//// no evidence of additional parameter values in function declaration
//function multiplySumES5(multiplyBy) {
//    // need to extract parameter values from arguments
//    var numbers = Array.prototype.slice.call(arguments,
//        multiplySumES5.length, // begin
//        arguments.length); // end
//    var total = 0;
//    numbers.forEach(function (item) {
//        total += item;
//    });
//    return multiplyBy * total;
//}
//
//// ES2015 call sum
//console.log('ES2015 call sum');
//console.log(sum(1, 2, 3));
//
//// ES5 call sumES5
//console.log('ES5 call sumES5');
//console.log(sumES5(1, 2, 3));
//
//// ES2015 call multiplySum
//console.log('ES2015 call multiplySum');
//console.log(multiplySum(3, 1, 2, 3));
//
//// ES5 call multiplySumES5
//console.log('ES5 call multiplySumES5');
//console.log(multiplySumES5(3, 1, 2, 3));
