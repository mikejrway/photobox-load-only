
// find the roots of a * x * x + b * x + c
function quadraticRoots(a = 1, b = -5, c = 6, iOK = true) {
    // function body omitted
    let determinant;
    determinant = b * b - 4 * a * c;
    if (determinant > 0) {
        const r1 = (-b + Math.sqrt(determinant)) / (2 * a);
        const r2 = (-b - Math.sqrt(determinant)) / (2 * a);
        return "Roots are: " + r1 + " and " + r2;
    }
    else if (determinant == 0) {
        const r1 = -b / (2 * a);
        return "Roots are: " + r1 + " and " + r1;
    }
    else {
        if (!iOK) return 'Setting doesn\'t allow imaginary result';
        const real = -b / (2 * a);
        const imag = Math.sqrt(-determinant) / (2 * a);
        return "Roots are: " + real + " + " + imag + "i and " + real + " - " + imag + "i";
    }
}

function quadraticRootsES5(a, b, c, iOK) {
    var a = a || 1;
    var b = b || -5;
    var c = c || 6;
    // would always be true
    var iOK = iOK || true;
    // need to test for undefined
    var iOK = typeof(iOK) === 'undefined' ? true : iOK;
}

function quadraticRootsOES5(options) {
    var a = options.a || 1;
    var b = options.b || -5;
    var c = options.c || 6;
    // would always be true
    var iOK = options.iOK || true;
    // need to test for undefined
    var iOK = typeof(options.iOK) === 'undefined' ? true : options.iOK;
}



// find the roots of a * x * x + b * x + c
function quadraticRootsO({a = 1, b = -5, c = 6, iOK = true} = {}) {
    let determinant;
    determinant = b * b - 4 * a * c;
    if (determinant > 0) {
        const r1 = (-b + Math.sqrt(determinant)) / (2 * a);
        const r2 = (-b - Math.sqrt(determinant)) / (2 * a);
        return "Roots are: " + r1 + " and " + r2;
    }
    else if (determinant == 0) {
        const r1 = -b / (2 * a);
        return "Roots are: " + r1 + " and " + r1;
    }
    else {
        if (!iOK) return 'Setting doesn\'t allow imaginary result';
        const real = -b / (2 * a);
        const imag = Math.sqrt(-determinant) / (2 * a);
        return "Roots are: " + real + " + " + imag + "i and " + real + " - " + imag + "i";
    }
}


// call against ES2015 parameters
console.log('ES2015 regular parameters:')
console.log(quadraticRoots(1, 5));
// call against ES2015 options object default
console.log('ES2015 options object:')
console.log(quadraticRootsO({b:5}));

// call against ES2015 parameters, imaginary not OK
console.log('ES2015 regular parameters, imaginary not OK:')
console.log(quadraticRoots(2, 5, 6, false));
// call against ES2015 options object default, imaginary not OK
console.log('ES2015 options object, imaginary not OK:')
console.log(quadraticRootsO({a:2, iOK:false}));