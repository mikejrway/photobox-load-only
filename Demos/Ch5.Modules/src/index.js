'use strict';
// Webpack is a module bundler - it traverses the module imports and exports to build a dependency tree

//import x from './exportVariable';
//import add from './add';
//import lora from './person';
//
//console.log(x);
//lora.age = add(x, 3);
//console.log(lora.age);

import {add} from './math';

console.log(add(3, 2));
//console.log(divide(3, 2));
//console.log(multiply(3, 2));
//console.log(subtract(3, 2));


// import from a default export
//import aValue from './defaultExportDemo';
//console.log(aValue);

// import from a named export
//import {aVariable, multiply} from './namedExportDemo';
//console.log(multiply(3, 4));
//console.log(aVariable);

// import using a wildcard
//import * as mod from './namedExportDemo';
//console.log(mod.multiply(4, 5));
//console.log(mod.aVariable);

// import from an aliased export
//import * as amod from './namedExportWithAliasDemo';
//console.log(amod.times(3, 5));
//console.log(amod.bVar);