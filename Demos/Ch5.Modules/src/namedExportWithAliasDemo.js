let aVariable = 'Hey there';

function multiply(a, b) {
    return a * b;
}

export {aVariable as bVar, multiply as times};