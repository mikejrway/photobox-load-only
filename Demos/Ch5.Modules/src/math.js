import add from './add';
import divide from './divide';
import multiply from './multiply';
import subtract from './subtract';

export {add, divide, multiply, subtract};