'use strict';

const PLAYER_NAME = "Your Name";
console.log(PLAYER_NAME);

// true values
console.log(PLAYER_NAME.startsWith('Y'));
console.log(PLAYER_NAME.endsWith('e'));
console.log(PLAYER_NAME.includes('our'));

// false values
console.log(PLAYER_NAME.startsWith('y'));
console.log(PLAYER_NAME.endsWith('Me'));
console.log(PLAYER_NAME.includes('mine'));


let welcomeMessage = `<h1>Welcome to Question Time ${PLAYER_NAME}</h1>`;
const APP_TARGET = document.getElementById('es-app');
APP_TARGET.innerHTML = welcomeMessage;