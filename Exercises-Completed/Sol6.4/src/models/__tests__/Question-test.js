jest.autoMockOff();

const Question = require('../Question').default;

describe('The Question class', () => {
    let q;
    beforeEach(() => {
       q = new Question('Test question', 0, 'correct', 'incorrect', 'other incorrect');
    });

    it('should set the questionText property', () => {
       expect(q.questionText).toBe('Test question');
    });
});