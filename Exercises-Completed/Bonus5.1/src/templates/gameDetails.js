import scoreDetails from './scoreDetails';
import questionDetails from './questionDetails';

export default function gameDetails(playerName) {
    return `<h1>Welcome to Question Time ${playerName}</h1>

${scoreDetails()}

${questionDetails('What is the new var?', 'let', 'int', 'string', 'double')}
`;
}
