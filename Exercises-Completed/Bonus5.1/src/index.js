'use strict';

import gameDetails from './templates/gameDetails';

const PLAYER_NAME = "Your Name";
const APP_TARGET = document.getElementById('es-app');
APP_TARGET.innerHTML = gameDetails(PLAYER_NAME);
