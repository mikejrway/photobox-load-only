import * as UI from './UI';

export default function gameDetails(game) {
    let {playerName, score, total, question} = game;
    let {questionText, answers} = question;
    return `<h1>Welcome to Question Time ${playerName}</h1>

${UI.scoreUI(score, total)}

${UI.questionUI(questionText, ...answers)}
`;
}
