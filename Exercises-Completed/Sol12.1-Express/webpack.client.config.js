var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: [
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'dist/public'),
        filename: 'bundle.[hash].js',
        publicPath: '/',
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loaders: ['babel'],
            include: path.join(__dirname, 'src')
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "Welcome to Question Time",
            template: "index.html",
            inject: "body"
        })
    ]
};
