import express from 'express';
import mongoose from 'mongoose';
//import questionBank from '../models/questionBank';
import QuestionModel from './QuestionModel';

let _app = express();
//let _questions = [...questionBank.get('Easy')];

_app.use(express.static('public'));
_app.set('json spaces', 2);
mongoose.connect('mongodb://127.0.0.1/questiontime');

_app.get('/questions', (req, res) => {
    QuestionModel.find((err, questions) => {
        if (err) {
            res.error(err);
        } else {
            res.json(questions);
        }
    });
    console.log('Request for /questions received');
});

_app.listen(process.env.PORT, () => {
   console.log('Application started');
});