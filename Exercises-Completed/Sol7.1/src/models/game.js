import questionBank from './questionBank';

let _score = 0, _total = 0,
    _questions = questionBank.values(),
    _question = _questions.next().value;
const PLAYER_NAME = 'Your Name';

let game = {
    get score() {
        return _score;
    },
    get total() {
        return _total;
    },
    get playerName() {
        return PLAYER_NAME;
    },
    get question() {
        return _question;
    },
    checkAnswer (answer) {
        if (_question.checkAnswer(answer)) {
            _score++;
        }
        _total++;
        _question = _questions.next().value;
    }
};

export default game;