'use strict';

import { gameUI } from './templates/UI';
import game from './models/game';

const APP_TARGET = document.getElementById('es-app');
APP_TARGET.innerHTML = gameUI(game);
