let _score = 0, _total = 0;
const PLAYER_NAME = 'Your Name';


let game = {
    get score() {
        return _score;
    },
    get total() {
        return _total;
    },
    get playerName() {
        return PLAYER_NAME;
    }
};

export default game;