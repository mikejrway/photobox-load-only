import gameDetails from './gameDetails';
import scoreDetails from './scoreDetails';
import questionDetails from './questionDetails';
import categoryDetails from './categoryDetails';

export {gameDetails as gameUI, scoreDetails as scoreUI, questionDetails as questionUI, categoryDetails as categoryUI};