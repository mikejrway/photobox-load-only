import questionBank from './questionBank';
import {notify} from '../utilities/nb';

let _score = 0, _total = 0, _over = false,
    _categories = [...questionBank.keys()],
    _selectedCategory = _categories[0],
    _questions = questionBank.get(_selectedCategory).values(),
    _question = _questions.next().value;
const PLAYER_NAME = 'Your Name';

let game = {
    get score() { return _score; },
    get total() { return _total; },
    get playerName() { return PLAYER_NAME; },
    get question() { return _question; },
    get over() { return _over; },
    get categories() { return _categories; },
    get selectedCategory() { return _selectedCategory; },
    setCategory (category) {
        _selectedCategory = category;
        _questions = questionBank.get(_selectedCategory).values();
        _question = _questions.next().value;
        notify();
    },
    checkAnswer (answer) {
        if (_question.checkAnswer(answer)) {
            _score++;
        }
        _total++;
        this.nextQuestion();
    },
    nextQuestion () {
        const item = _questions.next();
        if (item.done) {
            _over = true;
            notify();
            return;
        }
        _question = item.value;
        notify();
    }
};

export default game;