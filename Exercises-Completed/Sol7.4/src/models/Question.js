const _privateProperties = new WeakMap();

class Question {
    constructor(questionText, correctIndex, ...answers) {
        _privateProperties.set(this, {
           questionText,
            correctIndex,
            answers
        });
        //this._questionText = questionText;
        //this._correctIndex = correctIndex;
        //this._answers = answers;
    }

    get questionText() {
        return _privateProperties.get(this).questionText;
    }
    get correctIndex() {
        return _privateProperties.get(this).correctIndex;
    }
    get answers() {
        return _privateProperties.get(this).answers;
    }

    checkAnswer(answer) {
        return _privateProperties.get(this).correctIndex == answer;
    }
}

export default Question;