let _callbacks = new Set();

function addListener(callback) {
    _callbacks.add(callback);
}

function notify() {
    for (const callback of _callbacks) {
        callback();
    }
}

export {addListener, notify}