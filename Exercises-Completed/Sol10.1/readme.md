Code to integrate with a DynamoDB in memory database

1) Run Dynamo with java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb -inMemory

2) Kill dynamo to clear content (in memory!)

3) Run the import with npm run import 

4) Run the server with npm run server


Koa server is main
Import code is in run-import

EX - A: create the import code
EX - B: create the KOA server code to display the data