function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var Koa = require("koa");
var handlebars = require("koa-handlebars");

var app = new Koa();

app.use(handlebars({
  defaultLayout: "main"
}));

app.use((() => {
  var _ref = _asyncToGenerator(function* (ctx) {
    yield ctx.render("index", {
      title: "Test Page",
      name: "World"
    });
  });

  return function (_x) {
    return _ref.apply(this, arguments);
  };
})());

app.listen(3000);