/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Question {
    constructor(questionText, correctIndex, ...answers) {
        this._questionText = questionText;
        this._correctIndex = correctIndex;
        this._answers = answers;
    }

    get questionText() {
        return this._questionText;
    }
    get correctIndex() {
        return this._correctIndex;
    }
    get answers() {
        return this._answers;
    }

    checkAnswer(answer) {
        return this._correctIndex == answer;
    }
}

/* harmony default export */ __webpack_exports__["a"] = (Question);

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_koa__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_koa___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_koa__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_koa_handlebars__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_koa_handlebars___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_koa_handlebars__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_koa_route__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_koa_route___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_koa_route__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_koa_cors__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_koa_cors___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_koa_cors__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_koa_body__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_koa_body___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_koa_body__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_questionBank__ = __webpack_require__(3);
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }









let _questions = [...__WEBPACK_IMPORTED_MODULE_5__models_questionBank__["a" /* default */].get('Easy')];

var app = new __WEBPACK_IMPORTED_MODULE_0_koa___default.a();

app.use(__WEBPACK_IMPORTED_MODULE_1_koa_handlebars___default()({
  defaultLayout: "main"
}));

app.use(__WEBPACK_IMPORTED_MODULE_3_koa_cors___default()({
  methods: ['GET', 'POST']
}));

app.use(__WEBPACK_IMPORTED_MODULE_4_koa_body___default()());

app.use(__WEBPACK_IMPORTED_MODULE_2_koa_route___default.a.post('/question', (ctx, next) => {
  console.log(JSON.stringify(ctx.request.body));
  ctx.body = "OK";
  next();
}));

app.use(__WEBPACK_IMPORTED_MODULE_2_koa_route___default.a.get('/questions', (ctx, next) => {
  ctx.body = JSON.stringify(_questions);
  next();
}));

app.use(__WEBPACK_IMPORTED_MODULE_2_koa_route___default.a.get('/question/:id/:p2', (ctx, id, p2, next) => {
  console.log(`${id} ${p2}`);
  ctx.body = JSON.stringify(_questions[id]);
  next();
}));

app.use((() => {
  var _ref = _asyncToGenerator(function* (ctx) {
    yield ctx.render("home", {
      title: "Test Page",
      playerName: "Fred Flintstone",
      scores: {
        score: "5",
        total: "10",
        percent: "50%"
      }
    });
  });

  return function (_x) {
    return _ref.apply(this, arguments);
  };
})());

// app.use(async function (ctx, next) {
//   const start = new Date();
//   await next();
//   const ms = new Date() - start;
//   console.log(`${ctx.method} ${ctx.url} - ${ms}`);
//   ctx.body = `${ctx.body} ${ms} mS`;
// });


// app.use( async (ctx) => {
//   ctx.body = "Hello World";
// });

app.listen(3000);

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Question__ = __webpack_require__(0);


class MultiAnswerQuestion extends __WEBPACK_IMPORTED_MODULE_0__Question__["a" /* default */] {
    constructor(questionText, correctIndexes, ...answers) {
        super(questionText, -1, ...answers);
        this._correctIndexes = correctIndexes;
    }

    checkAnswer(answer) {
        return this._correctIndexes.sort().join() == answer.sort().join();
    }
}

/* harmony default export */ __webpack_exports__["a"] = (MultiAnswerQuestion);

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Question__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__MultiAnswerQuestion__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utilities_getData__ = __webpack_require__(4);




let _questionSet = new Set([new __WEBPACK_IMPORTED_MODULE_0__Question__["a" /* default */]('Which version of ES added get and set properties?', 1, 'ES3', 'ES5', 'ES6'), new __WEBPACK_IMPORTED_MODULE_0__Question__["a" /* default */]('What is the name of the new function syntax?', 2, 'dagger', 'spear', 'arrow', 'shotgun'), new __WEBPACK_IMPORTED_MODULE_0__Question__["a" /* default */]('Which of the following function definitions includes the rest operator?', 3, 'function () { }', 'function (a) { }', 'function (a = 1) { }', 'function (...a) { }'), new __WEBPACK_IMPORTED_MODULE_1__MultiAnswerQuestion__["a" /* default */]('Which parameter(s) has a/have default value(s): function (a = 0, b, c = 1, d) { }?', [0, 2], 'a', 'b', 'c', 'd')]);

let _mediumQuestionSet = new Set([new __WEBPACK_IMPORTED_MODULE_0__Question__["a" /* default */]('What would be displayed? console.log(Number.isNaN("abc"))', 1, 'true', 'false', '"abc"', 'null', 'undefined'), new __WEBPACK_IMPORTED_MODULE_0__Question__["a" /* default */]('What would be displayed? let a = NaN; console.log(Number.isNaN(a))', 0, 'true', 'false', '"abc"', 'null', 'undefined'), new __WEBPACK_IMPORTED_MODULE_0__Question__["a" /* default */]('What would be displayed? let a = 2.0; console.log(Number.isNaN(a))', 1, 'true', 'false', '"abc"', 'null', 'undefined'), new __WEBPACK_IMPORTED_MODULE_1__MultiAnswerQuestion__["a" /* default */]('What would be displayed? console.log([1, 2, 3].map((i) => i.toString()).join(""))', [0], '123', '1 2 3', '1,2,3', '[1,2,3]')]);

let _questionMap = new Map([['Easy', _questionSet], ['Medium', _mediumQuestionSet]]);

__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__utilities_getData__["a" /* default */])('http://api.questiontime.com/questions').then(JSON.parse).then(data => {
    data.forEach(item => {
        const { questionText, category, correctIndex, answers } = item;
        const ques = new __WEBPACK_IMPORTED_MODULE_0__Question__["a" /* default */](questionText, correctIndex, ...answers);
        _questionMap.get(category).add(ques);
    });
});

/* harmony default export */ __webpack_exports__["a"] = (_questionMap);

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
function getData(url) {
    let prom = new Promise((resolved, rejected) => {
        let req = new XMLHttpRequest();
        req.open('GET', url);
        req.addEventListener('load', () => {
            if (req.status == '200') {
                resolved(req.responseText);
            } else {
                rejected(req.responseText);
            }
        });
        req.send();
    });
    return prom;
}

/* harmony default export */ __webpack_exports__["a"] = (getData);

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("koa");

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("koa-body");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("koa-cors");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("koa-handlebars");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("koa-route");

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ })
/******/ ]);
//# sourceMappingURL=bundle.js.map