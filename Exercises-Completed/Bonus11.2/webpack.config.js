const path = require('path');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function(x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

module.exports = {
    devtool: 'source-map',
    entry: [
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/',
    },
    module: {
        rules: [{
            test: /\.js$/,
            include: [path.join(__dirname, 'src')],
            loader: 'babel-loader',
            options: {
                presets: ['es2017']
            }
        }]
    },
    externals: nodeModules

};

// var webpack = require('webpack');
// var path = require('path');
// var fs = require('fs');

// var nodeModules = {};
// fs.readdirSync('node_modules')
//     .filter(function(x) {
//         return ['.bin'].indexOf(x) === -1;
//     })
//     .forEach(function(mod) {
//         nodeModules[mod] = 'commonjs ' + mod;
//     });

// module.exports = {
//     entry: './src',
//     target: 'node',
//     output: {
//         path: path.join(__dirname, 'dist'),
//         filename: 'backend.js'
//     },
//     module: {
//         loaders: [
//             {test: /\.js$/, loaders: ['babel-loader'], include: path.join(__dirname, 'src')}
//         ]
//     },
//     externals: nodeModules,
//     plugins: [
//     ]
// }