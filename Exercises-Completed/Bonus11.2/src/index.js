import Koa from "koa";
import handlebars from "koa-handlebars";
import _ from "koa-route"
import cors from "koa-cors"
import koaBody from "koa-body"

import questionBank from './models/questionBank';

let _questions = [...questionBank.get('Easy')];

var app = new Koa();

app.use(handlebars({
  defaultLayout: "main"
}));

app.use(cors({
  methods: ['GET', 'POST']
}));

app.use(koaBody());

app.use(_.post('/question', (ctx, next) => {
  console.log(JSON.stringify(ctx.request.body));
  ctx.body="OK";
  next();
}));

app.use(_.get('/questions', (ctx, next) => {
  ctx.body=JSON.stringify(_questions);
  next();
}));

app.use(_.get(
  '/question/:id',
  (ctx, id, next) => {
    console.log(`${id} ${p2}`);
    ctx.body = JSON.stringify(_questions[id]);
    next();
  }));



app.use(async (ctx) => {
  await ctx.render("home", {
    title: "Test Page",
    playerName: "Fred Flintstone",
    scores: {
  	  score: "5",
      total: "10",
      percent: "50%"
    }
  });
});

// app.use(async function (ctx, next) {
//   const start = new Date();
//   await next();
//   const ms = new Date() - start;
//   console.log(`${ctx.method} ${ctx.url} - ${ms}`);
//   ctx.body = `${ctx.body} ${ms} mS`;
// });


// app.use( async (ctx) => {
//   ctx.body = "Hello World";
// });

app.listen(3000);



