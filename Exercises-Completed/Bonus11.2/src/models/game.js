import questionBank from './questionBank';
import {notify} from '../utilities/nb';
import wait from '../utilities/wait';

let _score = 0, _total = 0, _over = false,
    _answerResult = '',
    _categories = [...questionBank.keys()],
    _selectedCategory = _categories[0],
    _questions = questionBank.get(_selectedCategory).values(),
    _question = _questions.next().value;
const PLAYER_NAME = 'Your Name';

let game = {
    get score() { return _score; },
    get total() { return _total; },
    get answerResult() { return _answerResult; },
    get playerName() { return PLAYER_NAME; },
    get question() { return _question; },
    get over() { return _over; },
    get categories() { return _categories; },
    get selectedCategory() { return _selectedCategory; },
    setCategory (category) {
        _selectedCategory = category;
        _questions = questionBank.get(_selectedCategory).values();
        _question = _questions.next().value;
        notify();
    },
    checkAnswer (answer) {
        let correct = _question.checkAnswer(answer);
        if (correct) {
            _score++;
        }
        _total++;
        wait(100)
            .then(() => {
                _answerResult = correct ? '<h2>Correct!</h2>' : '<h2>Wrong</h2>';
            })
            .then(notify)
            .then(wait)
            .then(() => {
                _answerResult = '';
            })
            .then(notify)
            .then(this.nextQuestion);
    },
    nextQuestion () {
        const item = _questions.next();
        if (item.done) {
            _over = true;
            notify();
            return;
        }
        _question = item.value;
        notify();
    }
};

//wait(2000)
//    .then(game.nextQuestion)
//    .then(wait)
//    .then(game.nextQuestion)
//    .then(wait)
//    .then(game.nextQuestion)
//    .then(wait);

export default game;