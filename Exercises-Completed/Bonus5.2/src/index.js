'use strict';

import { gameUI } from './templates/UI';

const PLAYER_NAME = "Your Name";
const APP_TARGET = document.getElementById('es-app');
APP_TARGET.innerHTML = gameUI(PLAYER_NAME);
