'use strict';

import scoreDetails from './templates/scoreDetails';
import questionDetails from './templates/questionDetails';

const PLAYER_NAME = "Your Name";

let welcomeMessage = `<h1>Welcome to Question Time ${PLAYER_NAME}</h1>

${scoreDetails()}

${questionDetails('What is the new var?', 'let', 'int', 'string', 'double')}
`;
const APP_TARGET = document.getElementById('es-app');
APP_TARGET.innerHTML = welcomeMessage;
