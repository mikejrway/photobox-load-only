'use strict';

const PLAYER_NAME = "Your Name";

let welcomeMessage = `<h1>Welcome to Question Time ${PLAYER_NAME}</h1>

${scoreDetails()}

${questionDetails('What is the new var?', 'let', 'int', 'string', 'double')}
`;
const APP_TARGET = document.getElementById('es-app');
APP_TARGET.innerHTML = welcomeMessage;


function scoreDetails(score = 0, total = 0) {
    const percent = score * 100 / total;
    return `<h2>You have scored ${score} out of ${total} for ${Number.isNaN(percent) ? 0 : percent}%</h2>`;
}

function questionDetails(questionText, ...answers) {
    return `<h3>${questionText}</h3>

    <ul>
      ${answers.map((item, index) => `<li><input type='radio' name='answer' value='${index}'> ${item}</li>`).join('')}
    </ul>
    `
}