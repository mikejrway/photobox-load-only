jest.autoMockOff();

const Question = require('../Question').default;

describe('The Question class', () => {
    let q;
    beforeEach(() => {
       q = new Question('qt', 0, 'a', 'b', 'c', 'd');
    });

    it('should set the questionText property', () => {
       expect('qt').toEqual(q.questionText);
    });
    it('should return true with correct answer index', () => {
        expect(q.checkAnswer(0)).toBe(true);
    });
    it('should return false with incorrect answer index', () => {
        expect(q.checkAnswer(1)).toBe(false);
    });

});