jest.autoMockOff();

const game = require('../game').default;

describe('The game object', () => {
    it('should have score property', () => {
        expect(0).toEqual(game.score);
    });
    it('should have total property', () => {
        expect(0).toEqual(game.total);
    });
    it ('should increment score and total with a correct answer', () => {
        game.checkAnswer([0,2]);
        expect(1).toEqual(game.score);
        expect(1).toEqual(game.total);
    });
    it ('should increment total but not score with an incorrect answer', () => {
        game.checkAnswer([0,1]);
        expect(1).toEqual(game.score);
        expect(2).toEqual(game.total);
    });
});