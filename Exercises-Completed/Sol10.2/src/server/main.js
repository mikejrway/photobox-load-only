import Koa from "koa";
import _ from "koa-route"

import getQuestions from "./QuestionModel";

var app = new Koa();

app.use(_.get('/questions', async (ctx, next) => {
    try{
        ctx.body=JSON.stringify(await getQuestions("Easy"));
    } catch (e) {
        ctx.body = "It's not working. Check the server-side logs and make sure that DynamoDB is running!"
    }
  next();
}));

app.listen(3000);

/// If you get the DynamoDB error: make sure you have run it!
/// java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb -inMemory
// From the course directory


