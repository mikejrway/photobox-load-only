import AWS from "aws-sdk";
import getQuestionsForCategory from "../dynamodb/dynamo-query";
import configureAWS from "../dynamodb/config";


export default async function (category) {
    configureAWS();
    return getQuestionsForCategory(category);
};
