'use strict';

import { gameUI } from './templates/UI';
import game from './models/game';

const APP_TARGET = document.getElementById('es-app');
APP_TARGET.innerHTML = gameUI(game);

const CHECK_ANSWER_BUTTON = document.getElementById('check-answer-button');
CHECK_ANSWER_BUTTON.addEventListener('click', (e) => {
    e.preventDefault();
    const selectedRadio = document.querySelector('input[name=answer]:checked');
    const answer = Number.parseInt(selectedRadio.value);
    game.checkAnswer(answer);
    APP_TARGET.innerHTML = gameUI(game);
});