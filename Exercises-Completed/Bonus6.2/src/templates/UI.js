import gameDetails from './gameDetails';
import scoreDetails from './scoreDetails';
import questionDetails from './questionDetails';

export {gameDetails as gameUI, scoreDetails as scoreUI, questionDetails as questionUI};