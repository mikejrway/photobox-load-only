'use strict';

import { gameUI } from './templates/UI';
import game from './models/game';
import MultiAnswerQuestion from './models/MultiAnswerQuestion';

const APP_TARGET = document.getElementById('es-app');
APP_TARGET.innerHTML = gameUI(game);

const CHECK_ANSWER_BUTTON = document.getElementById('check-answer-button');
CHECK_ANSWER_BUTTON.addEventListener('click', (e) => {
    e.preventDefault();
    if (game.question instanceof MultiAnswerQuestion) {
        const selectedCheckboxes = document.querySelectorAll('input[name=answer]:checked');
        const answer = [...selectedCheckboxes].map((item) => Number.parseInt(item.value));
        game.checkAnswer(answer);
    } else {
        const selectedRadio = document.querySelector('input[name=answer]:checked');
        const answer = Number.parseInt(selectedRadio.value);
        game.checkAnswer(answer);
    }
    CHECK_ANSWER_BUTTON.disabled = game.over;
    APP_TARGET.innerHTML = gameUI(game);
});


