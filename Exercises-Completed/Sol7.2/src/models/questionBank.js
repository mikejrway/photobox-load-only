import Question from './Question';
import MultiAnswerQuestion from './MultiAnswerQuestion';

let _questionSet = new Set([
    new Question('Which version of ES added get and set properties?', 1, 'ES3', 'ES5', 'ES6'),
    new Question('What is the name of the new function syntax?', 2, 'dagger', 'spear', 'arrow', 'shotgun'),
    new Question('Which of the following function definitions includes the rest operator?', 3, 'function () { }', 'function (a) { }', 'function (a = 1) { }', 'function (...a) { }'),
    new MultiAnswerQuestion('Which parameter(s) has a/have default value(s): function (a = 0, b, c = 1, d) { }?', [0, 2], 'a', 'b', 'c', 'd')
]);

let _questionMap = new Map([
    ['Easy', _questionSet]

]);

export default _questionMap;