/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _express = __webpack_require__(1);

	var _express2 = _interopRequireDefault(_express);

	var _questionBank = __webpack_require__(2);

	var _questionBank2 = _interopRequireDefault(_questionBank);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

	var _app = (0, _express2.default)();
	var _questions = [].concat(_toConsumableArray(_questionBank2.default.get('Easy')));

	_app.set('json spaces', 2);

	_app.get('/questions', function (req, res) {
	   res.json(_questions);
	   console.log('Request for /questions received');
	});

	_app.listen(8081, function () {
	   console.log('Application started');
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = require("express");

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _Question = __webpack_require__(3);

	var _Question2 = _interopRequireDefault(_Question);

	var _MultiAnswerQuestion = __webpack_require__(4);

	var _MultiAnswerQuestion2 = _interopRequireDefault(_MultiAnswerQuestion);

	var _getData = __webpack_require__(5);

	var _getData2 = _interopRequireDefault(_getData);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

	var _questionSet = new Set([new _Question2.default('Which version of ES added get and set properties?', 1, 'ES3', 'ES5', 'ES6'), new _Question2.default('What is the name of the new function syntax?', 2, 'dagger', 'spear', 'arrow', 'shotgun'), new _Question2.default('Which of the following function definitions includes the rest operator?', 3, 'function () { }', 'function (a) { }', 'function (a = 1) { }', 'function (...a) { }'), new _MultiAnswerQuestion2.default('Which parameter(s) has a/have default value(s): function (a = 0, b, c = 1, d) { }?', [0, 2], 'a', 'b', 'c', 'd')]);

	var _mediumQuestionSet = new Set([new _Question2.default('What would be displayed? console.log(Number.isNaN("abc"))', 1, 'true', 'false', '"abc"', 'null', 'undefined'), new _Question2.default('What would be displayed? let a = NaN; console.log(Number.isNaN(a))', 0, 'true', 'false', '"abc"', 'null', 'undefined'), new _Question2.default('What would be displayed? let a = 2.0; console.log(Number.isNaN(a))', 1, 'true', 'false', '"abc"', 'null', 'undefined'), new _MultiAnswerQuestion2.default('What would be displayed? console.log([1, 2, 3].map((i) => i.toString()).join(""))', [0], '123', '1 2 3', '1,2,3', '[1,2,3]')]);

	var _questionMap = new Map([['Easy', _questionSet], ['Medium', _mediumQuestionSet]]);

	(0, _getData2.default)('http://api.questiontime.com/questions').then(JSON.parse).then(function (data) {
	    data.forEach(function (item) {
	        var questionText = item.questionText;
	        var category = item.category;
	        var correctIndex = item.correctIndex;
	        var answers = item.answers;

	        var ques = new (Function.prototype.bind.apply(_Question2.default, [null].concat([questionText, correctIndex], _toConsumableArray(answers))))();
	        _questionMap.get(category).add(ques);
	    });
	});

	exports.default = _questionMap;

/***/ },
/* 3 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Question = function () {
	    function Question(questionText, correctIndex) {
	        _classCallCheck(this, Question);

	        this._questionText = questionText;
	        this._correctIndex = correctIndex;

	        for (var _len = arguments.length, answers = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
	            answers[_key - 2] = arguments[_key];
	        }

	        this._answers = answers;
	    }

	    _createClass(Question, [{
	        key: "checkAnswer",
	        value: function checkAnswer(answer) {
	            return this._correctIndex == answer;
	        }
	    }, {
	        key: "questionText",
	        get: function get() {
	            return this._questionText;
	        }
	    }, {
	        key: "correctIndex",
	        get: function get() {
	            return this._correctIndex;
	        }
	    }, {
	        key: "answers",
	        get: function get() {
	            return this._answers;
	        }
	    }]);

	    return Question;
	}();

	exports.default = Question;

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _Question2 = __webpack_require__(3);

	var _Question3 = _interopRequireDefault(_Question2);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var MultiAnswerQuestion = function (_Question) {
	    _inherits(MultiAnswerQuestion, _Question);

	    function MultiAnswerQuestion(questionText, correctIndexes) {
	        var _Object$getPrototypeO;

	        _classCallCheck(this, MultiAnswerQuestion);

	        for (var _len = arguments.length, answers = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
	            answers[_key - 2] = arguments[_key];
	        }

	        var _this = _possibleConstructorReturn(this, (_Object$getPrototypeO = Object.getPrototypeOf(MultiAnswerQuestion)).call.apply(_Object$getPrototypeO, [this, questionText, -1].concat(answers)));

	        _this._correctIndexes = correctIndexes;
	        return _this;
	    }

	    _createClass(MultiAnswerQuestion, [{
	        key: 'checkAnswer',
	        value: function checkAnswer(answer) {
	            return this._correctIndexes.sort().join() == answer.sort().join();
	        }
	    }]);

	    return MultiAnswerQuestion;
	}(_Question3.default);

	exports.default = MultiAnswerQuestion;

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	function getData(url) {
	    var prom = new Promise(function (resolved, rejected) {
	        var req = new XMLHttpRequest();
	        req.open('GET', url);
	        req.addEventListener('load', function () {
	            if (req.status == '200') {
	                resolved(req.responseText);
	            } else {
	                rejected(req.responseText);
	            }
	        });
	        req.send();
	    });
	    return prom;
	}

	exports.default = getData;

/***/ }
/******/ ]);