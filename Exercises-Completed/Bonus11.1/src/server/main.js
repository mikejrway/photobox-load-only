import express from 'express';
import questionBank from '../models/questionBank';

let _app = express();
let _questions = [...questionBank.get('Easy')];

_app.set('json spaces', 2);

_app.get('/questions', (req, res) => {
   res.json(_questions);
    console.log('Request for /questions received');
});

_app.listen(8081, () => {
   console.log('Application started');
});